﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;

namespace XRMSRV3LIB
{
    #region Interfaces 

    [ServiceContract(
        SessionMode = SessionMode.Required, 
        CallbackContract = typeof(IBookServiceCallback))]
    public interface IBookService
    {
        [OperationContract(IsInitiating = true)]
        void SayHello(Student student);

        [OperationContract(
            IsOneWay = true, 
            IsInitiating = false, 
            IsTerminating = true)]
        void SayGoodbye();

        [OperationContract]
        void ApplyChanges();
    
        [OperationContract(Name = "GetById")]
        Book Get(int id);

        [OperationContract(Name = "GetByAuthorName")]
        Book[] Get(string author);
        
        [OperationContract]
        void Add(Book book);

        [OperationContract]
        [FaultContract(typeof(BookFaultModel))]
        Book Rent(Book book);

        [OperationContract]
        [FaultContract(typeof(BookFaultModel))]
        void GiveBack(Book book);
    }

    [ServiceContract]
    public interface IBookServiceCallback
    {
        [OperationContract(IsOneWay = true)]
        void OnCallback();
    }

    #endregion

    #region Models

    [DataContract]
    public enum PublicationType
    {
        [EnumMember]
        Literature,

        [EnumMember]
        Magazine,

        [EnumMember]
        Journal,

        [EnumMember]
        Comicbook
    }

    [DataContract]
    public class Book 
        : IEquatable<Book>
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Author { get; set; }

        [DataMember]
        public uint Year { get; set; }

        [DataMember]
        public PublicationType PublicationType { get; set; }

        public bool IsRented { get; set; }

        public DateTime? RentedSince { get; set; }

        public bool Equals(Book other)
        {
            return
                (Id == other.Id)
                || (Name.Equals(other.Name)
                    && Author.Equals(other.Author)
                    && Year == other.Year
                    && PublicationType == other.PublicationType);
        }
    }

    [DataContract]
    public class Student
        : IEquatable<Student>
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string FullName { get; set; }

        [DataMember]
        public ICollection<Book> Books { get; set; }
            => new List<Book>();

        public bool Equals(Student other)
        {
            return
                (Id == other.Id) && FullName.Equals(other.FullName);
        }
    }

    [DataContract]
    public class BookFaultModel
    {
        [DataMember]
        public string Message { get; set; }
    }

    #endregion

}
