﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using Client.ServiceReference1;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var instanceContext = new InstanceContext(new BookServiceCallback());
            var client = new BookServiceClient(instanceContext);
            client.SayHello(new Student());
            client.ApplyChanges();
            client.SayGoodbye();

            Console.WriteLine("Finished!");
            return;
        }
    }

    public class BookServiceCallback 
        : IBookServiceCallback
    {
        public void OnCallback()
        {
            Console.WriteLine("Student: Eh... Well, It's better to wait for the next time. Cheery oh!");
        }
    }
}
